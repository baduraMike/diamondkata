package diamond;

import java.util.ArrayList;

public class Diamond {

    private static ArrayList<Character> alphabet = new ArrayList<Character>();

    public static ArrayList<Character> createAlphabetArray() {
        for (int i = 65; i <= 90; i++) {         //[A-Z] list generator by ASCII codes
            alphabet.add((char) i);
        }
        return alphabet;
    }

    public static String printDiamondUpperHalf(char character) {

        int charIndex = alphabet.indexOf(character);
        int preChar = charIndex;
        int postChar = 0;

        String diamond = "";
        StringBuilder sB = new StringBuilder(diamond);

        for (int i = 0; i <= charIndex; i++) {

            if (i != charIndex) {
                preChar--;
                for (int j = preChar; j >= 0; j--) {
                    sB.append("-");
                }
            }

            sB.append(alphabet.get(i));

            if (i != 0) {
                postChar++;
                for (int k = 0; k < postChar; k++) {
                    sB.append("-");
                }
                postChar++;
            }

            if (i != 0) {
                sB.append(alphabet.get(i));
            }

            sB.append("\n");
        }
        return sB.toString();
    }

    public static String printDiamondLowerHalf(char character) {

        int charIndex = alphabet.indexOf(character);
        int preChar = 0;
        int postChar = (charIndex * 2) - 2;

        String diamond = "";
        StringBuilder sB = new StringBuilder(diamond);

        if (charIndex == 0) {
            sB.append(alphabet.get(charIndex));
        }

        for (int i = charIndex - 1; i >= 0; i--) {

            for (int j = preChar; j >= 0; j--) {
                sB.append("-");
            }
            preChar++;

            sB.append(alphabet.get(i));

            if (i != 0) {
                postChar -= 2;
                for (int k = postChar; k >= 0; k--) {
                    sB.append("-");
                }

            }

            if (i != 0) {
                sB.append(alphabet.get(i));
            }

            sB.append("\n");
        }
        return sB.toString();
    }

    public static String createDiamond(char character){

        String diamond = "";
        StringBuilder sB = new StringBuilder(diamond);

        sB.append(printDiamondUpperHalf(character));
        sB.append(printDiamondLowerHalf(character));

        return sB.toString();
        
    }

}
